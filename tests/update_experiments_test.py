#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun 13 20:48:26 2020

Emulates a call to update-experiments. Uses the `/mocksrv/data/` directory
provided in the repo. The output of this test should be:

updating experiment list from ../mocksrv/data
failed to create configuration for: ../mocksrv/data/MWAT1R25D
failed to create configuration for: ../mocksrv/data/MWAT1R26D
failed to create configuration for: ../mocksrv/data/MWAT1R27D
experiment list updated

3 of the configurations fail due to incomplete/missing `exp.ini` files.

@author: tdepe
"""

import glob
import os
import configparser
import uuid
import gi

gi.require_version('Apex', '1.0')

#pylint: disable=wrong-import-position

from gi.repository import Apex

from experiment.model import AppModel
from experiment.experiments import AppExperiments
from experiment.job import BundleExperimentJob, DeleteExperimentJob, UpdateExperimentsJob

#pylint: enable=wrong-import-position

data_path = os.path.abspath('../mocksrv/data')
test_expApp = AppExperiments(data_path)

# these don't seem to work right now, just print
# Apex.info("updating experiment list from %s" % exp.data_path)
print("updating experiment list from %s" % test_expApp.data_path)

# initialize experiments config
test_expApp.set_namespace(Apex.ConfigurationNamespace.EXPERIMENT)
test_expApp.set_id('exp-list')

# remove old configuration
if test_expApp.get_objects() is not None:
    for o in test_expApp.get_objects():
        test_expApp.remove_object(o)

# get path list (sort alphanumerically)
N = 0
exp_list = sorted(glob.glob(os.path.join(test_expApp.data_path, '*')))
for exp in exp_list:
    if '.zip' in exp:
        pass
    else:
        try:
            config = configparser.ConfigParser()
            ini_file = glob.glob(os.path.join(exp, 'exp.ini'))[0]
    
            # read the config from exp.ini file
            config.read(ini_file)
    
            # gather experiment information
            exp_id = os.path.split(exp)[-1]
            exp_status = config['experiment']['status']
            start_time = config['experiment']['start-time']
            stop_time = config['experiment']['end-time']
            image_count = config['experiment']['acq-image-count']
    
            # this may be necessary to find the bundle later, maybe shouldn't 
            # do it here, though.
            _, value = exp.split('srv/data', 1)
            exp_uri = value + ".zip"
            print("this: %s" % exp_uri)
    
            # populate experiment information dictionary
            obj = Apex.Object.new(str(uuid.uuid4()))
    
            obj.add_property(Apex.Property.new('exp-id', exp_id))
            obj.add_property(Apex.Property.new('exp-status', exp_status))
            obj.add_property(Apex.Property.new('exp-path', exp))
            obj.add_property(Apex.Property.new('exp-uri', exp_uri))
            obj.add_property(Apex.Property.new('exp-start-time', start_time))
            obj.add_property(Apex.Property.new('exp-stop-time', stop_time))
            obj.add_property(Apex.Property.new('exp-image-count', image_count))
    
            test_expApp.add_object(obj)
    
            N += 1
        except:
            # Apex.critical("failed to create configuration for:", exp)
            print("failed to create configuration for:", exp)

# add number of experiments
test_expApp.add_property(Apex.Property.new('n-exp', str(N)))

Apex.debug(test_expApp.serialize())

# finished
# Apex.info("experiment list updated")
print("experiment list updated")
