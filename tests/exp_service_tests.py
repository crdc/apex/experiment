#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 10 13:34:43 2019

@author: tdepe
"""

import os
import glob
import json
import tarfile

data_path = os.path.expanduser('~/work/dev/apex/mocksrv/data')

# initialize experiments config
experiments = {}
experiments['name'] = 'exp-list'
experiments["properties"] = {}
experiments["objects"] = []

# get path list (sort alphanumerically)
N = 0
exp_list = sorted(glob.glob(os.path.join(data_path, '*')))
for exp in exp_list:
    try:
        ini_file = glob.glob(os.path.join(exp, 'exp.ini'))[0]

        with open(ini_file, 'r') as f:
            json_ini = json.load(f)

        # gather experiment information
        exp_id = os.path.split(exp)[-1]
        exp_status = json_ini['exp-status']

        # populate experiment information dictionary
        e_dict = {}
        e_dict["id"] = "exp-%04d" % N

        e_dict["properties"] = {"exp-id": exp_id,
                                "exp-status": exp_status,
                                "exp-path": exp}
        e_dict["objects"] = []

        experiments["objects"].append(e_dict)

        N += 1
    except:
        pass

experiments["properties"] = {"n-exp": N}

# bundle a set of experiments into a tarfile
tar_id = 'exp-0001'

tar_index = next((index for (index, d) in enumerate(experiments['objects']) if d['id'] == tar_id), None)

tar_path = experiments['objects'][tar_index]['properties']['exp-path']
tar_name = os.path.basename(tar_path)
with tarfile.open(os.path.join(os.path.dirname(tar_path), tar_name + '.tar.gz'), 'w:gz') as tar:
    tar.add(tar_path, arcname=tar_name)

