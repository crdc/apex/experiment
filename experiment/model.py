class AppModel:
    def __init__(self):
        # Property backing fields
        self.props = {}
        self.props["system-status"] = "ready-download"

    def set_property(self, key, value):
        if key in self.props:
            if type(self.props[key]) == int:
                self.props.update({key: int(value)})
            elif type(self.props[key]) == float:
                self.props.update({key: float(value)})
            elif type(self.props[key] == str):
                self.props.update({key: value})

    def get_property(self, key):
        if key in self.props:
            return str(self.props.get(key))
        else:
            return None
