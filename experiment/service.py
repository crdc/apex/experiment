import os
import gi

gi.require_version('Apex', '1.0')

#pylint: disable=wrong-import-position

from gi.repository import Apex
from gi.repository import GLib

from experiment.model import AppModel
from experiment.experiments import AppExperiments
from experiment.job import BundleExperimentJob, DeleteExperimentJob, UpdateExperimentsJob

#pylint: enable=wrong-import-position

class App(Apex.Application):
    """App class is the service application"""
    __gtype_name__ = "App"

    def __init__(self, *args, **kwargs):
        super().__init__(*args,
                         application_id="org.apex.Experiment",
                         flags=0,
                         **kwargs)
        self.set_endpoint("tcp://localhost:7205")
        self.set_service("experiment")
        self.set_inactivity_timeout(10000)
        self.connect("property-changed", self.on_property_changed)
        # Communication properties
        self.props = []
        self.broker = None
        self.event_sink = None
        self.event_source = None
        self.experiments = AppExperiments(data_path='/srv/data/experiments')
        self.model = AppModel()
        self.experiments.update()
        self.__setup_properties()
        self.__setup_broker_client("tcp://localhost:7200")
        self.__setup_events("tcp://localhost:11005", "")

        # set in standalone mode
        # os.putenv('PLANTD_MODULE_STANDALONE', 'true')
        # os.putenv('PLANTD_MODULE_BROKER', 'tcp://*:7225')
        # os.putenv('PLANTD_MODULE_ENDPOINT', 'tcp://localhost:7225')

        # run update-experiments on startup
        # data_path = './mock/srv/data'
        # response = Apex.JobResponse.new()
        # job = UpdateExperimentsJob(self.experiments, data_path)
        # job.connect("event", self.publish_event)
        # job.do_task()
        # response.set_job(job)
        # Apex.info("experiment service initialization complete")

    def __setup_properties(self):
        for key, value in self.model.props.items():
            prop = Apex.Property.new(key, str(value))
            self.props.append(prop)
            self.add_property(prop)
            if self.has_property(key):
                Apex.info("added %s property" % key)

    def __setup_broker_client(self, endpoint):
        """Connect to the REQ/REP broker at the given endpoint."""
        self.broker = Apex.Client.new(endpoint)

    def __setup_events(self, endpoint, msg_filter):
        self.event_source = Apex.Source.new(endpoint, msg_filter)
        self.add_source("event", self.event_source)

    def publish_event(self, user_data, event):
        """Send an event to the connected bus sink for event messages"""
        Apex.info("sending event: %s" % event.serialize())
        try:
            self.send_event(event)
        except GLib.Error as err:
            if err.matches(Apex.apex_error_quark(),
                           Apex.ApexErrorEnum.NOT_CONNECTED):
                Apex.critical('Event source not connected')
            else:
                raise

    def on_property_changed(self, app, prop):
        Apex.info("changed (%s: %s)" % (prop.get_key(), prop.get_value()))
        self.model.set_property(prop.get_key(), prop.get_value())

    def do_activate(self):
        self.event_source.start()

    def do_shutdown(self):
        print("I was shut down")
        self.event_source.stop()

    def do_get_configuration(self):
        response = Apex.ConfigurationResponse.new()
        response.set_configuration(self.experiments)
        return response

    # apex_application_submit_job override
    def do_submit_job(self, job_name, job_value, job_properties):
        Apex.info("submit job: %s / %s" % (job_name, job_value))
        response = Apex.JobResponse.new()
        # TODO: execute different jobs for a given type
        if job_name == "bundle-experiments":
            bundle_ids = job_value.split(',')
            job = BundleExperimentJob(self.experiments, bundle_ids)
            job.connect("event", self.publish_event)
            response.set_job(job)

        elif job_name == "delete-experiments":
            delete_ids = job_value.split(',')
            job = DeleteExperimentJob(self.experiments, delete_ids)
            job.connect("event", self.publish_event)
            response.set_job(job)

        elif job_name == "update-experiments":
            try:
                data_path = job_properties['data-path']
            except:
                data_path = None
            if data_path == "":
                data_path = None

            job = UpdateExperimentsJob(self.experiments, data_path)
            job.connect("event", self.publish_event)
            response.set_job(job)
        else:
            pass
        return response
