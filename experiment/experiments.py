import glob
import os
import configparser
import uuid
import gi

gi.require_version('Apex', '1.0')

#pylint: disable=wrong-import-position

from gi.repository import Apex

#pylint: enable=wrong-import-position

class AppExperiments(Apex.Configuration):
    """AppExperiments class contains a list of experiments"""
    __gtype_name__ = "AppExperiments"

    def __init__(self, data_path, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.data_path = data_path

    def update(self, data_path=None):
        Apex.info("updating experiment configuration")
        Apex.info("data_path = %s" % data_path)

        # if required, update the data path
        if data_path is not None:
            self.data_path = data_path

        # crawl the data directory and get information from exp.ini files.
        Apex.info("updating experiment list from %s" % self.data_path)

        # initialize experiments config
        self.set_namespace(Apex.ConfigurationNamespace.EXPERIMENT)
        self.set_id('exp-list')

        # remove old configuration
        if self.get_objects() is not None:
            for o in self.get_objects():
                self.remove_object(o)

        # get path list (sort alphanumerically)
        N = 0
        exp_list = sorted(glob.glob(os.path.join(self.data_path, '*')))
        for exp in exp_list:
            if '.zip' in exp:
                pass
            else:
                exp_config_complete = True
                # open the ini file
                try:
                    config = configparser.ConfigParser()
                    ini_file = glob.glob(os.path.join(exp, 'exp.ini'))[0]
    
                    # read the config from exp.ini file
                    config.read(ini_file)
                except: 
                    Apex.critical("exp.ini missing for:", exp)

                # gather experiment information
                exp_id = os.path.split(exp)[-1]
                try:
                    exp_status = config['experiment']['status']
                except: 
                    # assume here if the exp.ini is missing a status, then run failed
                    exp_status = "EXP_FAILED"
                    exp_config_complete = False

                try:
                    start_time = config['experiment']['start-time']
                except:
                    start_time = "00"
                    exp_config_complete = False

                try:
                    stop_time = config['experiment']['end-time']
                except:
                    stop_time = "00"
                    exp_config_complete = False
                
                try:
                    image_count = config['experiment']['acq-image-count']
                except:
                    image_count = "0"
                    exp_config_complete = False
                
                # this may be necessary to find the bundle later, maybe 
                # shouldn't do it here, though.
                _, value = exp.split("srv/data", 1)
                exp_uri = value + ".zip"    
    
                # populate experiment information dictionary
                obj = Apex.Object.new(str(uuid.uuid4()))
    
                obj.add_property(Apex.Property.new('exp-id', exp_id))
                obj.add_property(Apex.Property.new('exp-status', exp_status))
                obj.add_property(Apex.Property.new('exp-path', exp))
                obj.add_property(Apex.Property.new('exp-uri', exp_uri))
                obj.add_property(Apex.Property.new('exp-start-time', start_time))
                obj.add_property(Apex.Property.new('exp-stop-time', stop_time))
                obj.add_property(Apex.Property.new('exp-image-count', image_count))
    
                self.add_object(obj)
    
                N += 1
                
                if not exp_config_complete:
                    Apex.info("exp.ini incomplete for:", exp)

        # add number of experiments
        self.add_property(Apex.Property.new('n-exp', str(N)))

        Apex.debug(self.serialize())

        # finished
        Apex.info("experiment list updated")
