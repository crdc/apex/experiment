"""Constants for experimentation service.

Events that should be picked up by something that cares about how program flow
is carried out.
"""

# Event IDs for state machine conditions
E_EXPERIMENT_STARTING = 1000
E_EXPERIMENT_STARTED = 1001
E_EXPERIMENT_STOPPING = 1002
E_EXPERIMENT_STOPPED = 1003

E_EXPERIMENT_LIST_UPDATED = 1004
E_EXPERIMENT_BUNDLING = 1005
E_EXPERIMENT_BUNDLED = 1006
E_EXPERIMENT_DELETING = 1007
E_EXPERIMENT_DELETED = 1008