import os
import configparser
import tarfile
import shutil
import time
import gi

gi.require_version('Apex', '1.0')

#pylint: disable=wrong-import-position

from gi.repository import Apex
from gi.repository import GObject

from experiment import constant

#pylint: enable=wrong-import-position

class UpdateExperimentsJob(Apex.Job):
    __gtype_name__ = "UpdateExperimentsJob"

    __gsignals__ = {
        'event': (GObject.SignalFlags.RUN_LAST, object, (object,))
    }

    def __init__(self, experiments, data_path=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.experiments = experiments
        self.data_path = data_path

    def do_task(self):
        # update experiment configuration
        self.experiments.update(data_path=self.data_path)

        event = Apex.Event.new_full(constant.E_EXPERIMENT_STARTING,
                                    "update",
                                    "update experiment list")
        self.emit("event", event)


class BundleExperimentJob(Apex.Job):
    '''
    Bundle an experiment directory.

    The `.ini` file will be checked to ensure the experiment is not:
    * acquiring
    * analyzing
    * already bundling

    should only be bundled if complete/failed/analyzed.
    '''
    __gtype_name__ = "BundleExperimentJob"

    __gsignals__ = {
        'event': (GObject.SignalFlags.RUN_LAST, object, (object,))
    }

    def __init__(self, experiments, bundle_ids, fmt='z', *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.experiments = experiments
        self.fmt = fmt
        self.bundle_ids = bundle_ids

    def do_task(self):
        valid_status = ['EXP_COMPLETE', 'EXP_FAILED', 'EXP_ANALYZED']

        Apex.info("bundle experiment for download")
        config = configparser.ConfigParser()

        # create a list of experiments requested that MAY be bundled
        valid_ids = []

        for bundle_id in self.bundle_ids:
            if self.experiments.has_object(bundle_id):
                Apex.message('bundle check experiment with ID: %s' % bundle_id)
                exp = self.experiments.lookup_object(bundle_id)
                prop_path = exp.lookup_property('exp-path')
                zip_path = prop_path.get_value()
                # check the experiment status
                config_file = os.path.join(zip_path, 'exp.ini')
                config.read(config_file)
                if config['experiment']['status'] in valid_status:
                    # set current experiment status to bundling
                    config['experiment']['status'] = 'EXP_BUNDLING'
                    with open(config_file, 'w') as fid:
                        config.write(fid)
                    # add to list of valid id's
                    valid_ids.append(bundle_id)
                else:
                    Apex.warning("Experiment %s cannot be bundled (status %s)" % (bundle_id, config['experiment']['status']))

        # bundle valid experiments
        for bundle_id in valid_ids:
            if self.experiments.has_object(bundle_id):
                Apex.message('bundling experiment with ID: %s' % bundle_id)
                exp = self.experiments.lookup_object(bundle_id)
                prop_path = exp.lookup_property('exp-path')
                prop_id = exp.lookup_property('exp-id')
                zip_path = prop_path.get_value()
                zip_name = prop_id.get_value()
                Apex.info('bundle exp %s (%s)' % (bundle_id, zip_name))

                if self.fmt == 'z':
                    shutil.make_archive(zip_path, format='zip', root_dir=zip_path)
                elif self.fmt == 't':
                    with tarfile.open(os.path.join(os.path.dirname(zip_path), zip_name + '.tar.gz'), 'w:gz') as tar:
                        tar.add(zip_path, arcname=zip_name, recursive=True)

                # set current experiment status to bundled
                config_file = os.path.join(zip_path, 'exp.ini')
                config.read(config_file)
                config['experiment']['status'] = 'EXP_BUNDLED'
                with open(config_file, 'w') as fid:
                    config.write(fid)

        # update the experiment configuration after bundling complete
        self.experiments.update()
        Apex.info("bundling complete")
        event = Apex.Event.new_full(constant.E_EXPERIMENT_STARTED,
                                    "bundle",
                                    "bundle experiment")
        self.emit("event", event)


class DeleteExperimentJob(Apex.Job):
    '''
    Delete an experiment directory.

    The `.ini` file will be checked to ensure the experiment is not:
    * bundling
    * analyzing

    should only be deleted if complete/failed/bundled.
    '''
    __gtype_name__ = "DeleteExperimentJob"

    __gsignals__ = {
        'event': (GObject.SignalFlags.RUN_LAST, object, (object,))
    }

    def __init__(self, experiments, delete_ids, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.experiments = experiments
        self.delete_ids = delete_ids

    def do_task(self):
        valid_status = ['EXP_COMPLETE', 'EXP_FAILED', 'EXP_ANALYZED', 'EXP_BUNDLED']

        Apex.info("delete experiment from data drive")
        config = configparser.ConfigParser()

        # create a list of experiments requested that MAY be bundled
        valid_ids = []

        for del_id in self.delete_ids:
            if self.experiments.has_object(del_id):
                Apex.message('delete check experiment with ID: %s' % del_id)
                exp = self.experiments.lookup_object(del_id)
                prop_path = exp.lookup_property('exp-path')
                del_path = prop_path.get_value()
                # check that the experiment can be deleted
                config_file = os.path.join(del_path, 'exp.ini')
                config.read(config_file)
                if config['experiment']['status'] in valid_status:
                    # set current experiment status to deleting
                    config['experiment']['status'] = 'EXP_DELETING'
                    with open(config_file, 'w') as fid:
                        config.write(fid)
                    valid_ids.append(del_id)
                else:
                    Apex.warning("Experiment %s cannot be deleted (status %s)" % (del_id, config['experiment']['status']))


        for del_id in valid_ids:
            if self.experiments.has_object(del_id):
                Apex.message('deleting experiment with ID: %s' % del_id)
                exp = self.experiments.lookup_object(del_id)
                prop_path = exp.lookup_property('exp-path')
                prop_id = exp.lookup_property('exp-id')
                del_path = prop_path.get_value()
                del_name = prop_id.get_value()
                Apex.info('delete exp %s (%s)' % (del_id, del_name))
                try:
                    shutil.rmtree(del_path)
                except:
                    Apex.critical('Error deleting experiment directory.')
                    return

                try:
                    os.remove(del_path + '.zip')
                except:
                    Apex.critical('Error deleting experiment archive.')

                time.sleep(1)
                Apex.info('Experiment %s deleted' % del_id)

        # update the experiment configuration after delete completes
        self.experiments.update()
        Apex.info("deleting complete")
        event = Apex.Event.new_full(constant.E_EXPERIMENT_STOPPED,
                                    "delete",
                                    "delete experiment")
        self.emit("event", event)
