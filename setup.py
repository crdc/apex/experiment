from setuptools import setup, find_packages
import experiment

setup(
    name='plantd-experiment',
    use_scm_version=True,
    author='Geoff Johnson',
    author_email='geoff.johnson@coanda.ca',
    description='Apex service for managing experiment data.',
    packages=find_packages(),
)
