# Experiment Service

Service for managing experiment data.

## Dependencies

* `apex-master`
* `apex-broker`
* `libapex`

## Install

```sh
python setup.py bdist_wheel
pip install dist/...
```

## Testing

Ensure all plantd services are running.

## Development

### Debian 10

Install dependencies.

Create a virtual env and install the experiment service in editable mode:
```bash
python3 -m venv env
source env/bin/activate
pip install --upgrade pip
pip install -e .
```

Copy the GI overrides for Apex:
```sh
cp /path/to/libplantd/apex/Apex.py ./env/lib64/python3.7/site-packages/gi/overrides/
```

You can start the experiment service in your virtual env by running:
```sh
experiment
```

To interact with the experiment service directly you can run the service in "standalone"
mode. This is accomplished by adding the following lines to the `Apex.Application`[^env]
initialization function in `service.py`:
```py
os.putenv('PLANTD_MODULE_STANDALONE', 'true')
os.putenv('PLANTD_MODULE_BROKER', 'tcp://*:7225')
os.putenv('PLANTD_MODULE_ENDPOINT', 'tcp://localhost:7225')
```
Note that the endpoints of the module and the standalone broker must have a matching port,
and must not conflict with any other active port.

Alternatively, may specify the environment variables from the command line when starting
the service:
```sh
PLANTD_MODULE_STANDALONE=true PLANTD_MODULE_BROKER=tcp://*:7225 PLANTD_MODULE_ENDPOINT=tcp://localhost:7225 experiment
```

This will allow interaction (for example, using `messenger`) without having a broker 
service running. Test the connections by launching `messenger` and using the following
configuration:
![alt text](doc/images/experiment-messenger-standalone.png "Testing 'update-experiments' job with messenger.")

The resulting output in the terminal running the experiment service should be:
```sh
./mocksrv/data/2020-02-05-17-40-30-0800-sample1
./mocksrv/data/2020-02-05-17-40-30-0800-sample2
./mocksrv/data/2020-02-05-17-40-30-0800-sample3
./mocksrv/data/2020-02-05-17-40-30-0800-sample4
./mocksrv/data/MWAT1R05D
```

[^env]: The environment variables mentioned are handled by the `apex-application` object
in `libplantd`. See https://gitlab.com/plantd/libplantd/-/blob/master/apex/core/apex-application.c#L212.

## Packaging

### Debian

Clone this repository and `cd` into it.

```sh
sudo apt install debhelper dh-python python3-all python3-setuptools
dpkg-buildpackage -b
```

The resulting packages will be in `../`.

## Notes

The experiment service provides meta-information about experiment runs (such as
fileserver location, filenames, number of images) and the current "status" of
experiments. The service relays information to the user via the experiment history
tab provided by [apex-react](https://gitlab.com/crdc/apex/apex-react).

The service provides functionality to package, download and delete
past experiments (and corresponding files/directories).

### Experiment Status ID's

The following status flags represent the possible states of an experiment:

* `EXP_COMPLETE` - indicates the experiment has finished acquiring images and writing data.
* `EXP_FAILED` - indicates there was a failure in the experiment (or analysis).
* `EXP_ANALYZING` - indicates the post-acquisition analysis is running.
* `EXP_ANALYZED` - indicates experiment has been successfully analyzed.
* `EXP_BUNDLING` - indicates the experiment data is being archived in preparation for download.
* `EXP_BUNDLED` - indicates the experiment archive is ready for download.
* `EXP_DELETING` - indicates the experiment files are being removed from the fileserver.
